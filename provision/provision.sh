#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive
sed -i "s/bullseye main/bullseye main contrib/"  /etc/apt/sources.list
apt-get update

echo "Update packages"
apt-get install wget gnupg2 -y
wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add -
apt-get install ca-certificates apt-transport-https wget curl -y
echo "deb https://packages.sury.org/php/ bullseye main" | tee /etc/apt/sources.list.d/php.list
apt-get update
apt-get upgrade -y


echo "Installing APT packages"
apt-get install lsb-release ca-certificates sudo wget curl git-core build-essential \
  vim elinks zip unzip software-properties-common apache2  \
  libapache2-mod-php8.1 php8.1-cli php8.1-curl php8.1-gd php8.1-mbstring php8.1-imagick \
  php8.1-mysql php8.1-dev php8.1-xml php8.1-intl php8.1-zip php8.1-bcmath php8.1-ldap php-dev \
  php8.1-soap mariadb-server autoconf automake build-essential  \
  -y --fix-missing


if [ ! -f /usr/local/bin/composer ]; then
  echo "Get composer"
  curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin
  mv /usr/bin/composer.phar /usr/bin/composer
  chmod a+x /usr/bin/composer
fi

echo "Installing postfix"
debconf-set-selections <<<"postfix postfix/mailname string dev.consorzisdb.it"
debconf-set-selections <<<"postfix postfix/main_mailer_type string 'Internet Site'"
apt-get install -y postfix


if [ "$(grep -c vagrant /etc/apache2/sites-enabled/000-default.conf)"=="0" ]; then
  echo "Configuring Apache2"

  sed -i 's/\/var\/www\/html/\/vagrant\/public/g' /etc/apache2/sites-enabled/000-default.conf
  sed -i 's/<\/VirtualHost>//g' /etc/apache2/sites-enabled/000-default.conf

  cat <<EOF >>/etc/apache2/sites-enabled/000-default.conf
    <Directory /vagrant/public>
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF

  a2enmod rewrite
  service apache2 restart
fi


if [ ! -f /usr/sbin/mysqld ]; then
    echo "Installing packages - mysql"
    DEBIAN_FRONTEND=noninteractive apt-get  -y install mysql-server
    sudo mysql -u root -e "update mysql.user set plugin = 'mysql_native_password' where User='root';"
    mysqladmin -u root password root
    service mysql restart
fi


echo "memory_limit = -1" >>/etc/php/8.1/apache2/php.ini

echo "deb http://deb.debian.org/debian bullseye-backports main contrib non-free" >/etc/apt/sources.list.d/cachefilesd.list
apt-get update
apt-get install cachefilesd -y
echo "RUN=yes" >/etc/default/cachefilesd


